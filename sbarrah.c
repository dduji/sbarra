//code released under the LGPL v3 license - www.gnu.org/licenses/lgpl-3.0.html

/*
hook lib
*/


#include "sbarrah.h"

#include <stdlib.h>
#ifdef _DEBUG
	#include <stdio.h>
#endif
#include <string.h>

#include "common.h"



//taskbar window
HWND window = NULL;
//sync mutex
HANDLE wait = NULL;

//desktop size
RECT deskrect = {0};


//how many pixels to distinguish if the taskbar is hidden or not
const int VIS_THRESHOLD = 5;



//---------------------------------------------------------------------------------------
//
//---------------------------------------------------------------------------------------
BOOL WINAPI DllMain(HINSTANCE instance, DWORD reason, LPVOID reserved)
{
  switch(reason){
    
		case DLL_PROCESS_ATTACH:{
			OutputDebugString("dll attach");
			if(DisableThreadLibraryCalls(instance) == FALSE){
				OutputDebugString("ERR: DisableThreadLibraryCalls failed"); //don't quit, not critical
			}

			if(GetWindowRect(GetDesktopWindow(),&deskrect) == 0) _simplethrow("ERR: can't get desktop rect");

			char modnam[MAX_PATH];
			if(!GetModuleFileName(NULL,modnam,MAX_PATH)) _simplethrow("ERR: can't get module name");
			if(strstr(modnam,THIS_EXE) == NULL){
				OutputDebugString("loading module is hook target");

				window = FindWindow(TARGET_CLASS,NULL);
				if(window == NULL) _simplethrow("ERR: can't find the taskbar window");

				wait = OpenMutex(MUTEX_ALL_ACCESS,FALSE,MUTEX_NAME);
				if(wait==NULL) _simplethrow("ERR: can't open the mutex");
				if(WaitForSingleObject(wait,1) != WAIT_OBJECT_0) _simplethrow("ERR: can't own the mutex");
				OutputDebugString("mutex owned");
			}
			else{
				OutputDebugString("loading module is not hook target");
			}
		}break;

		
    case DLL_PROCESS_DETACH:{
			OutputDebugString("dll detach");
      }break;
  }

  _simplecatch_
}





//---------------------------------------------------------------------------------------
// hook
//---------------------------------------------------------------------------------------
LRESULT CALLBACK hook_msgs(int ncode, WPARAM wparam, LPARAM lparam)
{
	//don't handle
	if(ncode<0 || ncode!=HC_ACTION) goto _next;


	CWPRETSTRUCT* m = (CWPRETSTRUCT*)lparam;
	HWND win = m->hwnd;
	if(win != window) goto _next;
	
	switch(m->message){
		case WM_WINDOWPOSCHANGED:{
			WINDOWPOS* barpos = (WINDOWPOS*)m->lParam;
			set_transp(win,barpos);
			}break;

		case WM_DESTROY:{
			OutputDebugString("window destroy");
			if(!ReleaseMutex(wait)) OutputDebugString("ERR: mutex is lost in limbo");
			else                    OutputDebugString("mutex released");
			}break;
	}

	//next handler in the hook chain
	_next:
	return CallNextHookEx((HHOOK)0,ncode,wparam,lparam);
}




//---------------------------------------------------------------------------------------
// sets or unsets the transparency, based on the position of the passed window (which
// should be the taskbar)
//---------------------------------------------------------------------------------------
BOOL WINAPI set_transp(HWND win, WINDOWPOS* pos)
{
	//read the position manually
	int top;
	if(pos==NULL){
		RECT wr;
		if(!GetWindowRect(win,&wr)) _simplethrow("ERR: can't get window rect");
		top = wr.top;
	}
	else{
		top = pos->y;
	}

	//current opacity settings
	BYTE curropc;
	if(!GetLayeredWindowAttributes(win,NULL,&curropc,NULL)) _simplethrow("ERR: can't get window opacity");
	
	//if the taskbar window top position is close to the desktop bottom, it means
	//it's "hidden", and it should be set to transparent. note that Windows doesn't
	//change the taskbar size, it only moves it down, mostly out of the screen
	if(abs(deskrect.bottom - top) <= VIS_THRESHOLD){
	  //only when the window is not already at the wanted opacity
		if(curropc > OPACITY_LOW){
			if(!SetLayeredWindowAttributes(win,0,OPACITY_LOW,LWA_ALPHA)) _simplethrow("ERR: can't hide");
			OutputDebugString("hidden");
		}
	}
	//otherwise it should be set to opaque
	else{
		if(curropc <= OPACITY_LOW){
			if(!SetLayeredWindowAttributes(win,0,OPACITY_FULL,LWA_ALPHA)) _simplethrow("ERR: can't show");
			OutputDebugString("shown");
		}
	}
	
  _simplecatch_
}



