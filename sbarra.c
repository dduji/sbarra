//code released under the LGPL v3 license - www.gnu.org/licenses/lgpl-3.0.html

/*
hook loader
*/


#ifndef _DEBUG
	#define NDEBUG
#endif

#define WIN32_LEAN_AND_MEAN
#include <windows.h>
#include <psapi.h>

#include <stdio.h>
#include <assert.h>
#include <string.h>
#include <stdlib.h>


#include "common.h"



BOOL enable_transp_win(HWND win);
BOOL disable_transp_win(HWND win);

HANDLE search_name_process(const char* srcnm);

void MessageBoxf(const char* fmt, ...);




BOOL (WINAPI *set_transp)(HWND,WINDOWPOS*) = NULL;

#define TIME_WAIT_DLL_READY 5000









//---------------------------------------------------------------------------------------
//
//---------------------------------------------------------------------------------------
int main(int argc, char* argv[])
{
  int _ret=0;
	HWND win = NULL;
	HHOOK hook = NULL;
	HANDLE wait = NULL;
	HANDLE oldproc = NULL;
	
	

  //for printf
  #ifdef _DEBUG
	AllocConsole();
	#endif
	
	
  OutputDebugString("searching previous instances");
	
	oldproc = search_name_process(THIS_EXE);
	if(oldproc!=NULL) _throw(1,"probable duplicate process found.\nkill it and then try again");
	CloseHandle(oldproc);
	


  OutputDebugString("loading dll");
	
	HMODULE dll = LoadLibrary(THIS_DLL);
	if(dll == NULL) _throw(10,"ERR: can't load the dll (%d)",GetLastError());

	HOOKPROC hook_msgs = NULL;
	hook_msgs = (HOOKPROC)GetProcAddress(dll,"hook_msgs");
	if(hook_msgs == NULL) _throw(11,"ERR: can't get hook_msgs (%d)",GetLastError());

	set_transp = (__typeof__(set_transp))GetProcAddress(dll,"set_transp");
	if(set_transp == NULL) _throw(12,"ERR: can't get set_transp (%d)",GetLastError());
   	
	

  OutputDebugString("creating wait mutex");
	
	wait = CreateMutex(NULL,FALSE,MUTEX_NAME);
	if(wait==NULL) _throw(20,"ERR: can't create mutex (%d)",GetLastError());
	
	
	
  OutputDebugString("configuring window");

	win = FindWindow(TARGET_CLASS,NULL);
	if(win == NULL) _throw(30,"ERR: can't find the window (%d)",GetLastError());

	if(!enable_transp_win(win)) _throw(31,"ERR: can't transp the window");
	
	
	
  OutputDebugString("hooking");

	DWORD th = GetWindowThreadProcessId(win,NULL);
	hook = SetWindowsHookEx(WH_CALLWNDPROCRET, hook_msgs, dll, th);
	if(hook==NULL) _throw(32,"ERR: can't hook (%d)",GetLastError());
	
	
	
  OutputDebugString("hooked. now waiting...");
	
	//wait for the DllMain to execute. it could be done with more sync objects,
	//but a simple wait seems sufficient
	Sleep(TIME_WAIT_DLL_READY);
	if(WaitForSingleObject(wait,INFINITE) == WAIT_FAILED) OutputDebugString("ERR: wait failed");
	

	
	OutputDebugString("..."THIS_NAME" is done");

	goto _final;
	_catch:
	assert(_ret!=0);
	
	_final:

	if(_ret!=0){
		if(hook!=NULL) UnhookWindowsHookEx(hook);
		if(win!=NULL) disable_transp_win(win);
	}
	if(wait!=NULL) CloseHandle(wait);
	if(oldproc!=NULL) CloseHandle(oldproc);
	

	
	return _ret;
}




//---------------------------------------------------------------------------------------
//
//---------------------------------------------------------------------------------------
BOOL enable_transp_win(HWND win)
{
	if(!IsWindow(win)) _simplethrow("ERR: not a window");

	LONG cs = GetClassLong(win,GCL_STYLE);
	if((cs&CS_CLASSDC) || (cs&CS_OWNDC)) _simplethrow("ERR: incompatible class style(s)");

	LONG xs = GetWindowLong(win,GWL_EXSTYLE);
	if(xs==0) _simplethrow("ERR: can't get window style");

	if( !(xs&WS_EX_LAYERED) ){
		OutputDebugString("window needs layered flag");
		xs |= WS_EX_LAYERED;
		SetLastError(0); //error checking gymnastics
		LONG r = SetWindowLong(win,GWL_EXSTYLE,xs);
		if(r==0 && GetLastError()!=0) _simplethrow("ERR: can't set the window style");
	}
	else{
		OutputDebugString("window already layered");
	}

	//as per MSDN, call SetLayeredWindowAttributes to allow the Get function to work
	if(!SetLayeredWindowAttributes(win,0,OPACITY_FULL,LWA_ALPHA)) _simplethrow("ERR: can't set first transp");
	if(!set_transp(win,NULL)) _simplethrow("ERR: can't set ~first transp");
	
	_simplecatch_
}


//---------------------------------------------------------------------------------------
//
//---------------------------------------------------------------------------------------
BOOL disable_transp_win(HWND win)
{
	if(!IsWindow(win)) _simplethrow("ERR: not a window");
	
	LONG xs = GetWindowLong(win,GWL_EXSTYLE);
	if(xs==0) _simplethrow("ERR: can't get window style");
	
	if( xs&WS_EX_LAYERED ){
		OutputDebugString("will remove layered flag");
		xs &= ~WS_EX_LAYERED;
		SetLastError(0);
		LONG r = SetWindowLong(win,GWL_EXSTYLE,xs);
		if(r==0 && GetLastError()!=0) _simplethrow("ERR: can't set the window style");
		
		//as per MSDN
		RedrawWindow(win,NULL,0,RDW_ALLCHILDREN);
	}
	else{
		OutputDebugString("layered flag already unset");
	}

	_simplecatch_
}







//---------------------------------------------------------------------------------------
// searches for a process, given it's exe name.
// the input string is not checked to be an exe.
// the function is not case sensitive.
// the caller has to close the returned handle.
// the current process is skipped.
//---------------------------------------------------------------------------------------
HANDLE search_name_process(const char* srcnm)
{
	DWORD* ids = NULL;
	size_t sz = sizeof(*ids)*10; //size of how many listable
	DWORD actualsz; //size of how many listed
	HANDLE foundp = NULL;
	
	
	do{	
		if(sz>0xA00000) _simplethrow("ERR: EnumProcesses mem boom");
		ids = realloc(ids,sz*=2);
		if(ids==NULL) _simplethrow("ERR: can't alloc");
		if(!EnumProcesses(ids,sz,&actualsz)) _simplethrow("ERR: can't enum processes");
	}while(sz==actualsz);

	const DWORD actual = actualsz/sizeof(*ids);

	//get the exe name of each process and compare it with the searched one
	char pnm[MAX_PATH];
	DWORD pcurr = GetCurrentProcessId();
	for(unsigned i=0; i<actual; i++){
		if(ids[i]==pcurr) continue; //skip the current process

		HANDLE h = OpenProcess(PROCESS_QUERY_LIMITED_INFORMATION,FALSE,ids[i]);
		//can't open? try the next anyway
		if(h==NULL) continue;
		
		if(GetProcessImageFileName(h,pnm,MAX_PATH)){
			//match
			if(strstr(pnm,srcnm) != NULL){ foundp = h; break; }
		}
		//not this one? try the next one
		CloseHandle(h);
	}
	
	goto _final;
	_catch:
	_final:
	if(ids!=NULL) free(ids);
	
	//found or NULL
	return foundp;
}




//---------------------------------------------------------------------------------------
// 
//---------------------------------------------------------------------------------------
void MessageBoxf(const char* fmt, ...)
{
  char s[300];
	
	va_list args;
  va_start(args,fmt);

  vsnprintf(s,300,fmt,args);
  va_end(args);

	OutputDebugString(s);
	MessageBox(NULL,s,THIS_NAME,MB_OK);
}
                  


