@if "%~1" == "" goto end
@if "%~1" == "dll" goto dll
@if "%~1" == "app" goto app
@goto end


:dll
gcc -std=c11 sbarrah.c -o sbarrah.dll -mdll -mwindows -Wl,--kill-at %2 %3 %4 %5
@goto end

:app
gcc -std=c11 sbarra.c -o sbarra.exe -mwindows -lpsapi %2 %3 %4 %5
@goto end


:end

