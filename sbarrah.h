//code released under the LGPL v3 license - www.gnu.org/licenses/lgpl-3.0.html

/*
hook lib
*/

#pragma once



#define WIN32_LEAN_AND_MEAN
#include <windows.h>




//dll keywords
#ifdef __GNUC__
	#define _dllimport __attribute__((dllimport))
	#define _dllexport __attribute__((dllexport))
#else
	#error "define the dll import/export thing"
#endif


//import or export
#ifdef _BUILD_IMPORT
	#define _dllsymbol _dllimport
#else
	#define _dllsymbol _dllexport
#endif


_dllsymbol LRESULT CALLBACK hook_msgs(int nCode, WPARAM wParam, LPARAM lParam);
_dllsymbol BOOL WINAPI set_transp(HWND win, WINDOWPOS* pos);


