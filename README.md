# sbarra

A program to make the Windows taskbar transparent when it auto-hides.

Currently it's for Windows 7 32-bit, it doesn't support the vertical bar, there is no options window, etc., but it can be easily modified to different needs.
It's compiled with gcc 8 (via mingw).
Enjoy :)

