//code released under the LGPL v3 license - www.gnu.org/licenses/lgpl-3.0.html

/*
common magicks
*/

//hide OutputDebugString on release builds
#ifndef _DEBUG
	#ifdef OutputDebugString
		#undef OutputDebugString
	#endif
	#define OutputDebugString(x)  ( (void)0 )
#endif


//shortcuts
#define _throw(n, msg, ...)  { MessageBoxf(msg,##__VA_ARGS__); _ret=n; goto _catch; }

#define _simplethrow(msg)  { OutputDebugString(msg); goto _catch; }

#define _simplecatch_ \
	goto _final; \
	_catch: \
  __attribute__((cold)); \
	return FALSE; \
	_final: \
	return TRUE; \
	__builtin_unreachable();
	
	



//class of the hooked window
#define TARGET_CLASS "Shell_TrayWnd"


//loader module
#define THIS_EXE "sbarra.exe"
//hook module
#define THIS_DLL "sbarrah.dll"


//friendly name
#define THIS_NAME "sbarra"



//opacity percentage, in bytes 0..255
#define OPACITY_LOW 26
#define OPACITY_NONE 0
#define OPACITY_FULL 255



//sync mutex name
#define MUTEX_NAME THIS_NAME"-hihi"

